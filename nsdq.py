import os
import os.path
import requests

import lxml.html as LH
import bs4
import re

import pandas as pd
import time

#since we're using Renaissance Technologies for our example we're naming the file as renaissance.csv
# reference: https://en.wikipedia.org/wiki/Renaissance_Technologies
file_name = 'renaissance.csv'

def reload(filename = file_name):
    returnDF = pd.DataFrame()
    if (os.path.isfile(filename)):
        returnDF = pd.read_csv(file_name, sep=',')
    return returnDF

def get_page(cSession, url):
    reTry = 10
    rows = []
    while(reTry > 0):
        responseHome = cSession.get(url)
        morePages = (responseHome.status_code == 200)
        if morePages:

            try:
                content = responseHome.text
                soup = bs4.BeautifulSoup(content, 'lxml')

                table = soup.find('div', attrs={'id':'total-positions-table'})

                table_body = table.find('table')
                rows = table_body.find_all('tr')
                break
            except:
                reTry = reTry - 1
                time.sleep(10)

    return rows

def getpositions(filename = file_name, institutional='renaissance-technologies-llc-63203', debug=False):
    rxLink = re.compile("https:\/\/www\.nasdaq\.com\/symbol\/(\w+)\/institutional-holdings")

    baseUrl = 'https://www.nasdaq.com/quotes/institutional-portfolio/' + institutional

    cSession = requests.session()

    cSession.headers.update({"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge/16.16299"})

    morePages = True
    isBreak = False    
    portfolio = []
    tickers = []
    url = baseUrl
    pageNumber = 1  

    positionDF = pd.DataFrame()

    #Iterating thru pages of the institutional portfolio

    while (morePages):
        if debug:
            print (url)
        rows = get_page(cSession, url)

        morePages = (len(rows)> 0)
        if morePages:
            for row in rows:
                if len(row.contents) == 13:
                    hrefobj = row.find("a")
                    if hrefobj != None:
                        hlink = hrefobj['href']
                        mxLink = rxLink.findall(hlink)
                        eachContent = row.text.split('\n') 

                        if mxLink != [] and len(eachContent) == 8:
                            ticker = mxLink[0]
                            companyname = eachContent[1]
                            clss = eachContent[2]
                            sharevalue = eachContent[3]
                            changevalue = eachContent[4]
                            percentchange = eachContent[5]
                            shares = eachContent[6]
                            ticker = ticker.upper()

                            if ticker in tickers:
                                isBreak = True
                                break
                            tickers.append(ticker)
                            portfolio.append((ticker, companyname, clss, sharevalue, changevalue, percentchange, shares))
            pageNumber = pageNumber + 1
            url = baseUrl + "?page=" + str(pageNumber)
        if isBreak:
            morePages = False

    positionDF = pd.DataFrame(portfolio, columns=['ticker', 'name', 'class', 'sharevalue', 'changevalue', 'percentchange', 'shares'])

    # save the dataframe so we can just reload it later
    positionDF.to_csv(filename, sep='\t', index=False)

    return positionDF