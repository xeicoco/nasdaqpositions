
# Stock Positions

Access institutional holding positions from NASDAQ website (https://www.nasdaq.com/symbol/ndaq/institutional-holdings), store in a dataframe and apply some data investigation. Then, maybe we can learn from their investments.

We're using:

* Pandas https://pandas.pydata.org/

* BeatifulSoup https://pypi.org/project/beautifulsoup4/

* Requests http://docs.python-requests.org/en/master/


## Load our module and pandas


```python
import nsdq
import pandas as pd
```

## Get positions of the Institutional Holding

Please visit first the NASDAQ website if there's an update in the disclosure of Institutional Holdings. And if you have a snapshot of the latest disclosed positions of the Institutional Holdings, please just do a reload to avoid sending too much request to NASDAQ website. 

For our example we're reading the positions of Renaissance Technologies (https://en.wikipedia.org/wiki/Renaissance_Technologies)



```python
renaissanceDF = nsdq.getpositions(institutional='renaissance-technologies-llc-63203', debug=True) 

```

    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=2
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=3
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=4
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=5
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=6
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=7
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=8
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=9
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=10
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=11
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=12
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=13
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=14
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=15
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=16
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=17
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=18
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=19
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=20
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=21
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=22
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=23
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=24
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=25
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=26
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=27
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=28
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=29
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=30
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=31
    https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203?page=32
    

## Reload the saved snapshot of the positions


```python
renaissanceDF = nsdq.reload(debug=True) 
```


Let's dump the snapshot in our dataframe.



```python
renaissanceDF
```


|ticker|name|class|sharevalue|changevalue|percentchange|shares|
|--- |--- |--- |--- |--- |--- |--- |
|VRSN|VERISIGN INC|COM|1,091,203|-1,858|(0.17)|6,915,100|
|ALGN|ALIGN TECHNOLOGY INC|COM|1,067,139|-23,519|(2.16)|2,894,800|
|DPZ|DOMINOS PIZZA INC|COM|1,014,983|-23,702|(2.28)|3,451,500|
|HUM|HUMANA INC|COM|938,401|-156,968|(14.33)|2,783,500|
|ARDM|NOVO-NORDISK A S|ADR|918,065|98,366|12|19,142,300|
|PANW|PALO ALTO NETWORKS INC|COM|848,007|-163,970|(16.2)|3,725,700|
|AAPL|APPLE INC|COM|845,108|845,108|New|3,818,833|
|VMW|VMWARE INC|CL A COM|803,679|229,794|40.04|5,289,800|
|PEP|PEPSICO INC|COM|784,992|-238,492|(23.3)|6,962,850|
|AMGN|AMGEN INC|COM|746,647|-23,890|(3.1)|3,803,600|
|VRTX|VERTEX PHARMACEUTICALS INC|COM|726,964|215,310|42.08|4,050,617|
|BMY|BRISTOL MYERS SQUIBB CO|COM|646,649|-244,081|(27.4)|10,547,200|
|ISRG|INTUITIVE SURGICAL INC|COM NEW|631,786|-165,476|(20.76)|1,174,410|
|CCI|CROWN CASTLE INTL CORP NEW|COM|597,842|-55,515|(8.5)|5,289,703|
|ABMD|ABIOMED INC|COM|585,647|151,675|34.95|1,551,423|
|CBOE|CBOE GLOBAL MARKETS INC|COM|541,494|71,330|15.17|5,190,200|
|DVMT|DELL TECHNOLOGIES INC|COM CL V|534,214|76,507|16.72|5,549,700|
|IDXX|IDEXX LABS INC|COM|526,316|19,653|3.88|2,169,929|
|JNJ|JOHNSON & JOHNSON|COM|513,618|337,244|191.21|3,740,300|
|PGR|PROGRESSIVE CORP OHIO|COM|488,626|80,130|19.62|7,094,900|
|CL|COLGATE PALMOLIVE CO|COM|482,532|-135,541|(21.93)|7,200,900|
|FE|FIRSTENERGY CORP|COM|450,398|84,462|23.08|12,007,408|
|AZPN|ASPEN TECHNOLOGY INC|COM|439,587|22,180|5.31|3,862,800|
|WCG|WELLCARE HEALTH PLANS INC|COM|432,567|-19,744|(4.37)|1,393,400|
|BBY|BEST BUY INC|COM|412,773|-20,642|(4.76)|5,279,100|
|NVDA|NVIDIA CORP|COM|405,015|-178,666|(30.61)|1,489,793|
|FB|FACEBOOK INC|CL A|402,299|402,299|New|2,467,488|
|MOH|MOLINA HEALTHCARE INC|COM|394,699|99,749|33.82|2,805,850|
|ETSY|ETSY INC|COM|388,254|16,574|4.46|8,236,200|
|AEP|AMERICAN ELEC PWR INC|COM|381,846|-38,012|(9.05)|5,253,800|
|...|...|...|...|...|...|...|
|SASR|SANDY SPRING BANCORP INC|COM|36,510|8,517|30.43|929,710|
|QDEL|QUIDEL CORP|COM|36,477|-4,772|(11.57)|507,687|
|AXTA|AXALTA COATING SYS LTD|COM|36,474|31,202|591.91|1,240,597|
|HPP|HUDSON PAC PPTYS INC|COM|36,417|22,521|162.06|1,105,900|
|DEA|EASTERLY GOVT PPTYS INC|COM|36,365|4,545|14.29|1,832,900|
|WLK|WESTLAKE CHEM CORP|COM|36,157|11,401|46.05|405,300|
|OFIX|ORTHOFIX MED INC|COM|36,138|1,121|3.20|677,500|
|DKS|DICKS SPORTING GOODS INC|COM|36,115|-301|(0.83)|948,400|
|UHS|UNIVERSAL HLTH SVCS INC|CL B|36,045|36,045|New|284,000|
|I|INTELSAT S A|COM|35,974|5,281|17.21|1,640,400|
|SMTC|SEMTECH CORP|COM|35,793|-21,746|(37.79)|630,721|
|MNTA|MOMENTA PHARMACEUTICALS INC|COM|35,772|-1,165|(3.15)|1,378,500|
|SJI|SOUTH JERSEY INDS INC|COM|35,657|1,276|3.71|1,064,400|
|BMTC|BRYN MAWR BK CORP|COM|35,585|2,318|6.97|733,719|
|BGNE|BEIGENE LTD|SPONSORED ADR|35,501|35,501|New|217,116|
|BUD|ANHEUSER BUSCH INBEV SA/NV|SPONSORED ADR|35,474|12,303|53.10|395,300|
|PAGP|PLAINS GP HLDGS L P|LTD PARTNR INT A|35,300|22,069|166.79|1,432,061|
|RDWR|RADWARE LTD|ORD|35,152|-274|(0.77)|1,295,200|
|FORR|FORRESTER RESH INC|COM|35,098|1,708|5.12|733,500|
|USM|UNITED STATES CELLULAR CORP|COM|35,041|2,768|8.58|828,000|
|USFD|US FOODS HLDG CORP|COM|34,953|-24,743|(41.45)|1,108,900|
|ETN|EATON CORP PLC|SHS|34,866|2,439|7.52|416,011|
|FRME|FIRST MERCHANTS CORP|COM|34,860|-421|(1.19)|729,129|
|XYL|XYLEM INC|COM|34,835|29,501|553.10|442,800|
|BRX|BRIXMOR PPTY GROUP INC|COM|34,449|-16,128|(31.89)|1,914,900|
|NXRT|NEXPOINT RESIDENTIAL TR INC|COM|34,367|2,343|7.32|1,078,000|
|TDC|TERADATA CORP DEL|COM|34,254|-6,457|(15.86)|883,300|
|AMWD|AMERICAN WOODMARK CORPORATION|COM|34,141|-29,947|(46.73)|406,200|
|MBUU|MALIBU BOATS INC|COM CL A|34,027|8,079|31.13|649,500|
|IDCC|INTERDIGITAL INC|COM|33,985|-6,455|(15.96)|416,995|


~~~
618 rows � 7 columns
~~~

# Example of analysis

In the following we wanted to see what's the latest positions and sort them by the number of shares.

Note: the sorting example below can also be done directly from NASDAQ: https://www.nasdaq.com/quotes/institutional-portfolio/renaissance-technologies-llc-63203/new?sortname=sharesheld&sorttype=1


```python
newPositions = renaissanceDF[(renaissanceDF.percentchange == 'New')]
newPositions['shares'] = newPositions['shares'].str.replace(',', '')
newPositions['shares'] = pd.to_numeric(newPositions['shares'], errors='coerce')
newPositions.sort_values(['shares'], ascending=[False])
```

|ticker|name|class|sharevalue|changevalue|percentchange|shares|
|--- |--- |--- |--- |--- |--- |--- |
|EVRG|EVERGY INC|COM|313,489|313,489|New|5377175|
|ERIC|ERICSSON|ADR B SEK 10|37,913|37,913|New|4423900|
|AAPL|APPLE INC|COM|845,108|845,108|New|3818833|
|MS|MORGAN STANLEY|COM NEW|175,074|175,074|New|3630727|
|FB|FACEBOOK INC|CL A|402,299|402,299|New|2467488|
|WY|WEYERHAEUSER CO|COM|77,377|77,377|New|2271781|
|MSFT|MICROSOFT CORP|COM|209,902|209,902|New|1939767|
|IVZ|INVESCO LTD|SHS|42,302|42,302|New|1758937|
|FL|FOOT LOCKER INC|COM|72,826|72,826|New|1558100|
|CRVL|CORVEL CORP|COM|86,398|86,398|New|1461900|
|AXP|AMERICAN EXPRESS CO|COM|138,084|138,084|New|1301700|
|LUV|SOUTHWEST AIRLS CO|COM|67,179|67,179|New|1085280|
|WLL|WHITING PETE CORP NEW|COM NEW|43,039|43,039|New|911465|
|ITW|ILLINOIS TOOL WKS INC|COM|111,494|111,494|New|792820|
|DLTR|DOLLAR TREE INC|COM|49,583|49,583|New|602903|
|CME|CME GROUP INC|COM CL A|102,146|102,146|New|587788|
|AME|AMETEK INC NEW|COM|44,179|44,179|New|565750|
|YUM|YUM BRANDS INC|COM|46,472|46,472|New|525700|
|SJM|SMUCKER J M CO|COM NEW|52,739|52,739|New|481503|
|AMG|AFFILIATED MANAGERS GROUP|COM|59,310|59,310|New|415600|
|KMB|KIMBERLY CLARK CORP|COM|44,479|44,479|New|385200|
|WYNN|WYNN RESORTS LTD|COM|41,544|41,544|New|323600|
|PAYC|PAYCOM SOFTWARE INC|COM|48,103|48,103|New|308550|
|UHS|UNIVERSAL HLTH SVCS INC|CL B|36,045|36,045|New|284000|
|BGNE|BEIGENE LTD|SPONSORED ADR|35,501|35,501|New|217116|



